# Homework assignment 1

## Question 1

### Problem 

Conditional probability 


Suppose that if $\theta = 1$, then y has a normal distribution with mean 1 and standard deviation $\sigma$, and if $\theta = 2$, then y has a normal distribution with
mean 2 and standard deviation $\sigma$. Also, suppose $Pr(\theta = 1) = 0.5 and Pr(\theta = 2) = 0.5$.\\

a

For $\sigma = 2$, write the formula for the marginal probability density for y and sketch it.\\

### Solution

a

The marginal distribution is defined by 
$$P(y) = Pr(\theta=1)\mathcal{N}(1,\,4)\,+Pr(\theta=2)\mathcal{N}(2,\,4)\, $$
= 
$$ 0.5 \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {y - 1 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 1 } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}} + 0.5 \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {y - 2 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 2 } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}}$$
$$= 0.5 \frac{1}{{2 \sqrt {2\pi } }}e^{{{ - \left( {y - 1 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 1 } \right)^2 } {8 }}} \right. \kern-\nulldelimiterspace} {8 }}} + 0.5 \frac{1}{{2 \sqrt {2\pi } }}e^{{{ - \left( {y - 2 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 2 } \right)^2 } {8 }}} \right. \kern-\nulldelimiterspace} {8 }}}$$

b

From Bayes theorem 
$$P(\theta=1|Y=1)=\frac{P(\theta=1)P(Y=1|\theta=1)}{P(Y=1)} $$
Using section a for Y's marginal distribution. 
$$=\frac{0.5\times \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {y - 1 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 1 } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}}}{0.5 \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {y - 1 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 1 } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}} + 0.5 \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {y - 2 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 2 } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}}}=\frac{1}{1+e^{\frac{-1}{8}}}$$


c

The Posterior distribution is 
$$P(\theta | Y) =\frac{0.5\times \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {y - 1 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 1 } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}}}{0.5 \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {y - 1 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 1 } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}} + 0.5 \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {y - 2 } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {y - 2 } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}}}
$$ 
$$ = \frac{e^{-(y-1)^2/2\sigma^2}}{e^{-(y-1)^2/2\sigma^2}+e^{-(y-2)^2/2\sigma^2}}$$


Thus for different values of y, the posterior of $\theta$ changes as seen in the figure 

e.g. for any large positive y as $\sigma \rigtarrow 0 $

## Question 2

### Problem

### Solution
a 

$$Pr(favorite\ wins | point\ spread = 8) = \frac{\#favorite\ Wins}{\#games\ of\ 8\ point\ spread} = \frac{8}{12} = \frac{2}{3}$$

$$Pr(favorite\ wins\ by\ at\ least\ 8\ points  | point\ spread = 8) =\frac{5}{12}$$

$$Pr(favorite\ wins\ by\ at\ least\ 8\ points  | point\ spread = 8\ and\ favorite\ wins) =\frac{5}{8}$$

b 
Consider the (outcome − point spread) samples : 
$$−15, −13, −11, −11, -7, -2, -1, 5, 7,
8, 12,\ and\ 13 $$ and assume a normal distribution with $\mu=-1.5, \sigma=9.67$, then
$$Pr(favorite\ wins | point\ spread = 8)=\int_{-8}^{\infty}  \mathcal{N}(-1.5,9.67)dx$$
$$Pr(favorite\ wins\ by\ at\ least\ 8\ points  | point\ spread = 8) = \int_{0}^{\infty}  \mathcal{N}(-1.5,9.67) dx$$

And by Bayes
$$Pr(favorite\ wins\ by\ at\ least\ 8\ points  | point\ spread = 8\ and\ favorite\ wins)$$ 
$$ = \frac{Pr(favorite\ wins\ by\ at\ least\ 8\ points  | point\ spread = 8)}{Pr(favorite\ wins | point\ spread = 8)}$$
$$= \frac{\int_{-0}^{\infty}  \mathcal{N}(-1.5,9.67)dx}{\int_{-8}^{\infty}  \mathcal{N}(-1.5,9.67)dx}$$


## Question 3


### Problem

Let $b$ denote the event he had a brother.
\newline
$Twin_f$ the event he had a fraternal twin. \newline
$Twin_i$ the event he had an identical twin.
\newline
$Twin_{any}$ the event he had any twin.\newline


### Solution


$Pr(Twin_{any},b) = Pr(Twin_i,b) + Pr(Twin_f,b) = \frac{1}{300} + \frac{1}{250} = \frac{11}{1500}$ \newline


From Bayes theorem we know that:\newline


$Pr(Twin_i|Twin_{any},b) =\frac{Pr(Twin_{any},b|Twin_i)\cdot Pr(Twin_i)}{Pr(Twin_{any}, b)} = \frac{1\cdot \frac{1}{300} }{\frac{11}{1500}} = \frac{5}{11} $
